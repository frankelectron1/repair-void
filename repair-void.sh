#!/bin/bash
#ste -e

SCRIPT_PATH=$(dirname $(realpath $0))
ICON_IMAGE=/usr/share/void-artwork/void-transparent.png
WIDTH=450
HEIGHT=300

SET_KBLAYOUT="de"
SET_KBVARIANTA="latin1"
SET_KBVARIANTB="nodeadkeys"

TITLE="Repair-Void"

pass_word () {
    PASS_FAIL=0

    while true; do
        PASS="$(zenity --password)"

        if ! [ $? == 0 ]; then
            PASS=""
            exit
            break

        elif [ -z $PASS ] || ! sudo -kSp '' [ 1 ] <<<$PASS 2>/dev/null; then
            PASS_FAIL=$(($PASS_FAIL+1))
            if [ $PASS_FAIL == 3 ]; then
                PASS=""
                zenity --error --text="3 X Wrong Password! Exit!"
                exit
                break
            fi

            zenity --error --text="Wrong Password! Try again"
            continue

        else
            echo -e $PASS | sudo -S xbps-install -S | zenity --progress --pulsate --auto-close --no-cancel --title="$TITLE" --text="Updating Package Source"
            break
        fi
    done

    PASS=""
}


update_repair_void () {
    sudo git -C $SCRIPT_PATH pull https://gitlab.com/frankelectron1/repair-void.git | zenity --progress --pulsate --auto-close --no-cancel --title="$TITLE" --text="Updating Repair-Void"

    notify-send 'Update complete'
    zenity --info --text="Update complete. Restart Repair-Void."

    sudo -k
    exec repair-void
}


continue_action () {
    zenity --question --text="$1"

    if ! [ $? == 0 ]; then
        main_menu
    fi
}

set_xkb () {
	if [ -f /usr/share/X11/xorg.conf.d/40-libinput.conf ]; then
		LINE_N=`sudo grep -n '/usr/share/X11/xorg.conf.d/40-libinput.conf' -e 'libinput keyboard catchall' | cut -d":" -f1`
		LINE_N=$(( $LINE_N+4 ))
		if ! [ -z $SET_KBLAYOUT ]; then
			OPTION='Option "XkbLayout" "'$SET_KBLAYOUT'"'
			sudo sed -i.bak "${LINE_N}i\\	$OPTION\\" /usr/share/X11/xorg.conf.d/40-libinput.conf
			if ! [ -z $SET_KBVARIANTA ]; then
				VARIANT="$SET_KBVARIANTA"
				if ! [ -z $SET_KBVARIANTB ]; then
					VARIANT+=",$SET_KBVARIANTB"
				fi
				LINE_N=$(( $LINE_N+1 ))
				OPTION='Option "XkbVariant" "'$VARIANT'"'
				sudo sed -i.bak "${LINE_N}i\\	$OPTION\\" /usr/share/X11/xorg.conf.d/40-libinput.conf
				unset VARIANT
			fi
			unset OPTION
		fi
		unset LINE_N
	fi

	KEYLA+="$SET_KBLAYOUT-$SET_KBVARIANTA-$SET_KBVARIANTB"
	LINE_N=`sudo grep -n '/etc/rc.conf' -e 'KEYMAP=' | cut -d":" -f1`
		sudo sed -i -e ${LINE_N}c"KEYMAP=$KEYLA" /etc/rc.conf
    unset LINE_N
    unset KEYLA
}


specials_menu () {
    while true; do
        ans=`zenity --list \
        --title="Actions" --width=$WIDTH --height=$HEIGHT --text="Options" --cancel-label Back --ok-label Execute --title="$TITLE" --hide-header --radiolist \
        --column="Dot" --column="Actoins" --column="Description" \
        1 Special-Repair "Comming soon" `

        if [ $? = 1 ]; then
            break
        fi

        if [ "$ans" == "Special-Repair" ]; then
            zenity --info --text="Comming soon!"
        fi

    done
}


main_menu () {
    while true; do
        ans=`zenity --list \
        --title="Actions" --width=$WIDTH --height=$HEIGHT --text="Options" --cancel-label Quit --ok-label Execute --title="$TITLE" --hide-header --radiolist \
        --column="Dot" --column="Actoins" --column="Description" \
        1 Update "Update System" \
        2 Deutsch "German keyboard" \
        3 Reinstall "Reinstall packages installed explicitly" \
        4 Recongigure "Reconfigure all packages" \
        5 Specials "Special repair menu" \
        6 Update-Repair-Void "Update Repair-Void" \
        7 Reboot "Reboot system" `

        # cancel
        if [ $? = 1 ]; then
            sudo -k
            break
        fi


        if [ "$ans" == "Update" ]; then
            continue_action "Update System?"
            sudo xbps-install -Suy xbps | zenity --progress --pulsate --auto-close --no-cancel --title="$TITLE" --text="Updating XBPS"
            sudo xbps-install -Suy | zenity --progress --pulsate --auto-close --no-cancel --title="$TITLE" --text="Updating System"
            notify-send 'System ist aktuell'
        fi

        if [ "$ans" == "Clean" ]; then
            continue_action "Clean XBPS chache?"
            sudo xbps-remove -O | zenity --progress --pulsate --auto-close --no-cancel --title="$TITLE" --text="Clean XBPS cache"
            notify-send 'XBPS chache is clean'
        fi

        if [ "$ans" == "Deutsch" ]; then
            continue_action "German keyboard?"
            set_xkb
        fi

        if [ "$ans" == "Reinstall" ]; then
            continue_action "Reinstall packages installed explicitly?"
            sudo xbps-install -f $(xbps-query -m | cut -d' ' -f2) | zenity --progress --pulsate --auto-close --no-cancel --title="$TITLE" --text="Reinstall"
            notify-send 'Reinstall successfull'
        fi

        if [ "$ans" == "Recongigure" ]; then
            continue_action "Reconfigure all packages?"
            sudo xbps-recongigure -fa | zenity --progress --pulsate --auto-close --no-cancel --title="$TITLE" --text="Recongigure"
            notify-send 'Reconfigure successfull'
        fi

        if [ "$ans" == "Specials" ]; then
            specials_menu
        fi

        if [ "$ans" == "Update-Repair-Void" ]; then
            continue_action "Update Repair-Void?"
            update_repair_void
            notify-send 'Update Repair-Void successfull'
        fi

        if [ "$ans" == "Reboot" ]; then
            continue_action "Reboot system?"
            sudo reboot
        fi

    done
}

pass_word
main_menu

exit 0
