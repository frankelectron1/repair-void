#!/bin/bash
#ste -e

SCRIPT_PATH=$(dirname $(realpath $0))
INSTALL_DIR=/opt/repair-void
ICON_IMAGE=/usr/share/void-artwork/void-logo.svg

continue_install () {
    zenity --question --text="This will install Repair-Void."

    if ! [ $? == 0 ]; then
        PASS=""
        exit 4
    fi
}


pass_word () {
    PASS_FAIL=0

    while true; do
        PASS="$(zenity --password)"

        if ! [ $? == 0 ]; then
            PASS=""
            exit
            break

        elif [ -z $PASS ] || ! sudo -kSp '' [ 1 ] <<<$PASS 2>/dev/null; then
            PASS_FAIL=$(($PASS_FAIL+1))
            if [ $PASS_FAIL == 3 ]; then
                PASS=""
                zenity --error --text="3 X Wrong Password! Exit!"
                exit
                break
            fi

            zenity --error --text="Wrong Password! Try again"
            continue

        else
            echo -e $PASS | sudo -S xbps-install -S | zenity --progress --pulsate --auto-close --no-cancel --title="$TITLE" --text="Updating Package Source"
            break
        fi
    done

    PASS=""
}


requirements () {
    if ! [ -f /bin/git ]; then
        sudo xbps-install -S git
        notify-send 'Install git. Pleas wait'
    fi

    if ! [ -f /bin/zenity ]; then
        sudo xbps-install -S zenity
        notify-send 'Install zenity. Pleas wait'
    fi
}


install_repair_void () {
    sudo git clone https://gitlab.com/frankelectron1/repair-void.git $INSTALL_DIR/ | zenity --progress --pulsate --auto-close --no-cancel --title="Install Repair-Void" --text="Clone Repository"

    if ! [ -f /usr/local/bin/repair-void ]; then
        sudo ln -s $INSTALL_DIR/repair-void.sh /usr/local/bin/repair-void
    fi

    if ! [ -f ~/.local/share/applications/Repair-Void.deskto ]; then
        tee ~/.local/share/applications/Repair-Void.desktop &>/dev/null <<"EOF"
[Desktop Entry]
Name=Repair-Void
Comment=Repair-Void
Type=Application
Exec=repair-void
Icon=emblem-system-symbolic
Terminal=false
Categories=System;
EOF
    fi

    notify-send 'Installation complete'
    zenity --info --text="Installation complete"
}


continue_install
pass_word
requirements
install_repair_void

exit 0
