# repair-void

repair-void is a bash script with GUI that solve problems on Void Linux.
It is primarily written for a friend.


## Installation

Download and execute install-repair-void.sh https://gitlab.com/frankelectron1/repair-void/-/blob/main/install-repair-void.sh


## Usage

After inastallation search for Repair-Void in your App menu or execute repair-void in Terminal.

