#!/bin/bash
#ste -e

SCRIPT_PATH=$(dirname $(realpath $0))

continue_uninstall () {
    zenity --question --text="This will uninstall Repair-Void."

    if ! [ $? == 0 ]; then
        PASS=""
        exit 4
    fi
}


pass_word () {
    PASS_FAIL=0

    while true; do
        PASS="$(zenity --password)"

        if ! [ $? == 0 ]; then
            PASS=""
            exit
            break

        elif [ -z $PASS ] || ! sudo -kSp '' [ 1 ] <<<$PASS 2>/dev/null; then
            PASS_FAIL=$(($PASS_FAIL+1))
            if [ $PASS_FAIL == 3 ]; then
                PASS=""
                zenity --error --text="3 X Wrong Password! Exit!"
                exit
                break
            fi

            zenity --error --text="Wrong Password! Try again"
            continue

        else
            echo -e $PASS | sudo -S xbps-install -S | zenity --progress --pulsate --auto-close --no-cancel --title="$TITLE" --text="Updating Package Source"
            break
        fi
    done

    PASS=""
}


uninstall_repair_void () {
    sudo rm -f /usr/local/bin/repair-void
    sudo rm -f ~/.local/share/applications/Repair-Void.desktop
    sudo rm -rf /opt/repair-void
}


continue_uninstall
pass_word
uninstall_repair_void | zenity --progress --pulsate --auto-close --no-cancel --title="Unnstall Repair-Void" --text="Unnstall Repair-Void"

notify-send 'Uninstallation complete'
zenity --info --text="Uninstallation complete"

exit 0
